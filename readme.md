Présentation du projet :\
Ceci est un projet maven ayant pour but de permettre l'implémentation de QCM pour le site waytolearnx.com.\
Une fois lancé, l'utilisateur a le choix entre plusieurs QCM et peut répondre avec les touches du clavier.\
Une fois le QCM fini, le programme lui propose les corrections des questions où il a fait des erreurs.\
Si une question a plusieurs réponses, l'ordre des réponses données importe peu, tant qu'elles sont séparées d'un espace.

Mode d'Emploi :\
Utiliser la fonction "package" de maven afin de construire le projet. \
Pour exécuter le programme en ligne de commande, entrer "java -jar WayToLearnX_Pommerie-1.0-SNAPSHOT.jar" dans le terminal.