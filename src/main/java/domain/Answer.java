package domain;

public class Answer {
    String contentAnswer = "";

    public Answer(Answer choices) {
        this.contentAnswer = choices.contentAnswer;
    }

    public String getContentAnswer() {
        return contentAnswer;
    }

    public void setContentAnswer(String contentAnswer) {
        this.contentAnswer = contentAnswer;
    }

    public Answer() {

    }
}
