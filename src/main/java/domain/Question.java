package domain;

public class Question {
    int number = 0;
    String enonce = "";
    String goodAnswers = "";
    String[] possibleAnswers = null;
    String explanation = "";

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public String getGoodAnswers() {
        return goodAnswers;
    }

    public void setGoodAnswers(String goodAnswers) {
        this.goodAnswers = goodAnswers;
    }

    public String[] getPossibleAnswers() {
        return possibleAnswers;
    }

    public void setPossibleAnswers(String[] possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Question(int number, String enonce, String goodAnswers, String[] possibleAnswers, String explanation) {
        this.number = number;
        this.enonce = enonce;
        this.goodAnswers = goodAnswers;
        this.possibleAnswers = possibleAnswers;
        this.explanation = explanation;
    }

    //on convertir le numéro de la question en la lettre qu'on affichera
    private String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char) (i + 64)) : null;
    }

    public void printQuestion() {
        System.out.print("= Question\040" + this.number + "\n" + this.enonce + "\n\n");

        //ce compteur va nous permettre de changer la lettre en fonction de la réponse
        int nbAnswerPrinted = 1;

        for (String answer : possibleAnswers) {
            if (!answer.equals("")) {
                System.out.println(getCharForNumber(nbAnswerPrinted) + " -\040" + answer);
                nbAnswerPrinted++;
            }
        }
    }

}
