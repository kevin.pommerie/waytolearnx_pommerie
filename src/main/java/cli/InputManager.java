package cli;

import java.util.Scanner;

public class InputManager {

    private static final Scanner scanner = new Scanner(System.in);

    private static final InputManager INSTANCE = new InputManager();

    public static InputManager getInstance() {
        return INSTANCE;
    }

    private InputManager() {
    }

    //nous sert uniquement dans le menu principal
    public int getChoice() {
        Integer choice = null;

        do {
            try {
                /* We don't use nextInt because it is a very confusing api that can not consume System.in input buffer
                   and produce infinite loops behavior */
                choice = Integer.parseInt(scanner.nextLine());

            } catch (Exception e) {
                System.out.println("Choix invalide");
            }

        } while (choice == null);

        return choice;
    }

    //on récupère une ligne afin de permettre d'entrer plusieurs réponses
    public String getMultipleChoices() {
        String choices = "";

        do {
            try {
                choices = scanner.nextLine();

            } catch (Exception e) {
                System.out.println("Choix invalide");
            }

        } while (choices == null);

        return choices;
    }
}
