package cli.screen;

import cli.InputManager;

public class MainMenuScreen implements Screen {

    public Screen render() {
        System.out.println("""
                Quel QCM souhaitez vous réalisez ?
                1 - Java - Programmation orientée objet
                2 - Java - Les collections - partie 1
                3 - Quitter l’application
                                
                Votre choix :""");

        int choice = InputManager.getInstance().getChoice();

        return switch (choice) {
            case 1 -> new JavaPOOScreen();
            case 2 -> new JavaCollectionsPart1Screen();
            case 3 -> new GoodByeScreen();
            default -> this;
        };
    }
}
