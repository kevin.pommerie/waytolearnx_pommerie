package cli.screen;

import cli.InputManager;
import domain.Answer;
import domain.Question;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public abstract class QuizScreen implements Screen {
    int score = 0;
    Answer choices = new Answer();
    ArrayList<Question> wrongQuestions = new ArrayList<>();
    ArrayList<Answer> wrongAnswers = new ArrayList<>();

    public void answerQuestion(int number, String enonce, String[] possibleAnswers, String goodAnswers, String explanation) {
        Question theQuestion = new Question(number, enonce, goodAnswers, possibleAnswers, explanation);
        theQuestion.printQuestion();

        System.out.println("\nVotre réponse :");
        choices.setContentAnswer(InputManager.getInstance().getMultipleChoices());

        //toUpperCase permet de gérer le cas où l'utilisateur répond en minuscules
        if (sameChars(choices.getContentAnswer().toUpperCase(Locale.ROOT), goodAnswers)) {
            System.out.println("bonne réponse!");
            score += 1;
        } else {
            System.out.println("mauvaise réponse");
            wrongQuestions.add(theQuestion);
            wrongAnswers.add(new Answer(choices));
        }
    }

    //fonction permettant de voir si les deux strings contiennent les mêmes caractères
    private boolean sameChars(String firstParam, String secondParam) {
        char[] first = firstParam.toCharArray();
        char[] second = secondParam.toCharArray();

        Arrays.sort(first);
        Arrays.sort(second);

        return Arrays.equals(first, second);
    }


    public Screen printScoreAndWrongAnswers() {
        if (score == 10) {
            System.out.println("= Résultat du QCM\n\n" + "Bravo ! Vous avez fait un sans faute et obtenu un 10/10.\n");
            return new MainMenuScreen();

        } else {
            System.out.print("""
                                            
                    = Résultat du QCM
                            
                    Votre note est de\040""" + score + "/10.\n");
                    //l'utilisation de \040 garantit la présence d'un espace insécable

            System.out.println("Voulez-vous voir les réponses des questions auxquelles vous avez échouées (Y/N) ?");
            choices.setContentAnswer(InputManager.getInstance().getMultipleChoices());

            //on permet de répondre "oui" par "y" et par "o"
            if (choices.getContentAnswer().equalsIgnoreCase("Y") || choices.getContentAnswer().equalsIgnoreCase("O")) {
                return new AnswersScreen(wrongQuestions, wrongAnswers);

            } else if (choices.equals("N")) {
                return new MainMenuScreen();

            } else {
                System.out.println("choix invalide, nous vous renvoyons vers le menu principal");
                return new MainMenuScreen();
            }
        }
    }
}
