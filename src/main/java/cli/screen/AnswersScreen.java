package cli.screen;

import domain.Answer;
import domain.Question;

import java.util.ArrayList;

public class AnswersScreen implements Screen {
    ArrayList<Answer> wrongAnswers = null;
    ArrayList<Question> wrongQuestions = null;


    public AnswersScreen(ArrayList<Question> wrongQuestions, ArrayList<Answer> wrongAnswers) {
        this.wrongQuestions = wrongQuestions;
        this.wrongAnswers = wrongAnswers;
    }

    @Override
    public Screen render() {
        System.out.println("= Réponses du QCM\n");

        for (int i = 0; i < wrongQuestions.size(); i++) {
            wrongQuestions.get(i).printQuestion();

            System.out.println("\nVotre réponse : " + wrongAnswers.get(i).getContentAnswer());
            System.out.println("Réponse correcte :" + wrongQuestions.get(i).getGoodAnswers() + "\n");
            System.out.println("Explication : " + wrongQuestions.get(i).getExplanation() + "\n");
        }

        return new MainMenuScreen();
    }
}
