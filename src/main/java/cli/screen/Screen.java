package cli.screen;

public interface Screen {

    /**
     * Launch a screen workflow.
     *
     * @param firstScreen First screen to display
     */
    static void launchWorkflow(Screen firstScreen) {
        Screen screenToDisplay = firstScreen;

        while (screenToDisplay != null) {
            screenToDisplay = screenToDisplay.render();

            if (screenToDisplay != null) {
                // Display screen separation
                System.out.print("""
                                                
                        ----
                        """);
            }
        }
    }

    /**
     * Display a screen on console and return the next screen.
     *
     * @return The next screen or null if application should stop.
     */
    Screen render();
}
