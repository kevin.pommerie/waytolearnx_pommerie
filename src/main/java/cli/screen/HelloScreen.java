package cli.screen;

public class HelloScreen implements Screen {
    @Override
    public Screen render() {
        System.out.print("""
                === QCM WayToLearnX ===
                                               
                """);

        return new MainMenuScreen();
    }
}
