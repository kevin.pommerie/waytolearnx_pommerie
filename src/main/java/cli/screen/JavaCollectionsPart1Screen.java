package cli.screen;

public class JavaCollectionsPart1Screen extends QuizScreen {

    @Override
    public Screen render() {
        System.out.println("=== QCM Java – Les collections – Partie 1\n");

        String[] possibleAnswers = {"java.awt", "java.net", "java.util", "java.lang"};
        answerQuestion(1, "Lequel de ces packages contient toutes les classes de collection?", possibleAnswers, "C", "Les classes du Framework Collections se trouvent dans le package java.util.");

        possibleAnswers = new String[]{"Queue", "Stack", "Array", "Map"};
        answerQuestion(2, "Laquelle de ces classes ne fait pas partie du framework collection en Java?", possibleAnswers, "D", "La classe Map ne fait pas partie du framework collection.");

        possibleAnswers = new String[]{"SortedMap", "SortedList", "Set", "List"};
        answerQuestion(3, "Laquelle de ces interfaces ne fait pas partie du framework collection en Java?", possibleAnswers, "B", "L’interface SortedList ne fait pas partie du framework collection.");

        possibleAnswers = new String[]{"refresh()", "delete()", "reset()", "clear()"};
        answerQuestion(4, "Laquelle de ces méthodes supprime tous les éléments d’une collection?", possibleAnswers, "D", "La méthode clear() supprime tous les éléments d’une collection.");

        possibleAnswers = new String[]{"Un groupe d’objets", "Un groupe d’interfaces", "Un groupe de classes", "Aucune de ces réponses n’est vraie."};
        answerQuestion(5, "Qu’est-ce que Collection en Java?", possibleAnswers, "A", "Une collection est un objet qui représente un groupe d’objets.");

        possibleAnswers = new String[]{"Collection", "Set", "Group", "List"};
        answerQuestion(6, "Laquelle de ces interfaces ne fait pas partie du framework collection en Java?", possibleAnswers, "C", "Group ne fait pas partie du framework collection.");

        possibleAnswers = new String[]{"Set", "List", "Map", "Toutes les réponses sont vraies"};
        answerQuestion(7, "Quelle interface n’autorise pas les doublons?", possibleAnswers, "A", "Voir : Différence entre List, Set et Map en java");

        possibleAnswers = new String[]{"Array", "Arrays", "ArrayList", "Toutes les réponses sont vraies"};
        answerQuestion(8, "Laquelle de ces classes de collection a la capacité d’évoluer de façon dynamique?", possibleAnswers, "C", "ArrayList est un tableau redimensionnable qui implémente l’interface List.");

        possibleAnswers = new String[]{"des valeurs nulles", "une clé nulle", "Toutes les réponses sont vraies", "Aucune de ces réponses n’est vraie."};
        answerQuestion(9, "Un HashMap autorise _____________", possibleAnswers, "C", "HashMap autorise une clé nulle et les valeurs nulles (une seule clé nulle est autorisée car deux clés ne sont pas autorisées). Par contre Hashtable n’autorise pas les clés nulles ou les valeurs nulles.");

        possibleAnswers = new String[]{"La redéfinition de la méthode equals", "La redéfinition de la méthode hashCode", "Toutes les réponses sont vraies", "Aucune de ces réponses n’est vraie."};
        answerQuestion(10, "L’efficacité d’un HashMap peut être garantie avec __________", possibleAnswers, "C", "HashMap s’appuie sur la méthode equals() et hashCode() pour comparer les clés et les valeurs.");

        return printScoreAndWrongAnswers();
    }
}
