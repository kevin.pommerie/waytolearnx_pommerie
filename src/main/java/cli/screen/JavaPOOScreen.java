package cli.screen;

public class JavaPOOScreen extends QuizScreen {

    @Override
    public Screen render() {
        System.out.println("=== QCM Java - Programmation orientée objet\n");

        String[] possibleAnswers = {"Héritage", "Encapsulation", "Polymorphisme", "Compilation"};
        answerQuestion(1, "Lequel des éléments suivants n’est pas un concept POO en Java?", possibleAnswers, "D", "Il existe 4 concepts POO en Java. Héritage, encapsulation, polymorphisme et abstraction.");

        possibleAnswers = new String[]{"Au moment de l’exécution", "Au moment de la compilation", "Au moment du codage", "Au moment de l’exécution"};
        answerQuestion(2, "Quand la surcharge de méthode est-elle déterminée?", possibleAnswers, "B", "La surcharge est déterminée au moment de la compilation.");

        possibleAnswers = new String[]{"Quand il y a plusieurs méthodes avec le même nom mais avec une signature de méthode différente et un nombre ou un type de paramètres différent", "Quand il y a plusieurs méthodes avec le même nom, le même nombre de paramètres et le type mais une signature différente", "Quand il y a plusieurs méthodes avec le même nom, la même signature, le même nombre de paramètres mais un type différent", "Quand il y a plusieurs méthodes avec le même nom, la même signature mais avec différente signature"};
        answerQuestion(3, "Quand la surcharge ne se produit pas?", possibleAnswers, "B", "La surcharge survient lorsque plusieurs méthodes ont le même nom mais un constructeur différent et aussi quand la même signature mais un nombre différent de paramètres et / ou de type de paramètre.");

        possibleAnswers = new String[]{"Polymorphisme", "Encapsulation", "Abstraction", "Héritage"};
        answerQuestion(4, "Quel concept de Java est un moyen de convertir des objets du monde réel en termes de classe?", possibleAnswers, "C", "L’abstraction est un concept de définition des objets du monde réel en termes de classes ou d’interfaces.");

        possibleAnswers = new String[]{"Polymorphisme", "Encapsulation", "Abstraction", "Héritage"};
        answerQuestion(5, "Quel concept de Java est utilisé en combinant des méthodes et des attributs dans une classe?", possibleAnswers, "B", "L’encapsulation est implémentée en combinant des méthodes et des attributs dans une classe. La classe agit comme un conteneur de propriétés d’encapsulation.");

        possibleAnswers = new String[]{"Agrégation", "Composition", "Encapsulation", "Association"};
        answerQuestion(6, "Comment ça s’appelle si un objet a son propre cycle de vie?", possibleAnswers, "D", "C’est une relation où tous les objets ont leur propre cycle de vie. Cela se produit lorsque de nombreuses relations sont disponibles, au lieu de One To One ou One To Many.");

        possibleAnswers = new String[]{"Agrégation", "Composition", "Encapsulation", "Association"};
        answerQuestion(7, "Comment s’appelle-t-on dans le cas où l’objet d’une classe mère est détruit donc l’objet d’une classe fille sera détruit également?", possibleAnswers, "B", "La composition se produit lorsque l’objet d’une classe fille est détruit si l’objet de la classe mère est détruit. La composition est également appelée une agrégation forte.");

        possibleAnswers = new String[]{"Agrégation", "Composition", "Encapsulation", "Association"};
        answerQuestion(8, "Comment s’appelle-t-on l’objet a son propre cycle de vie et l’objet d’une classe fille ne dépend pas à un autre objet d’une classe mère?", possibleAnswers, "A", "L’agrégation se produit lorsque les objets ont leur propre cycle de vie et que l’objet d’une classe fille peut être associé à un seul objet d’une classe mère.");

        possibleAnswers = new String[]{"Vrai", "Faux"};
        answerQuestion(9, "La surcharge d’une méthode peut remplacer l’héritage et le polymorphisme?", possibleAnswers, "A", "L’agrégation se produit lorsque les objets ont leur propre cycle de vie et que l’objet d’une classe fille peut être associé à un seul objet d’une classe mère.");

        possibleAnswers = new String[]{"final", "private", "abstract", "protected", "public"};
        answerQuestion(10, "Quels keywords sont utilisés pour spécifier la visibilité des propriétés et des méthodes ?", possibleAnswers, "B D E", "les mots clés utilisés pour spécifier la visibilité des propriétés et des méthodes sont : private, protected, public.");

        return printScoreAndWrongAnswers();
    }
}

