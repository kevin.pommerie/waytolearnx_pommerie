package cli;

import cli.screen.QuizScreen;
import cli.screen.Screen;
import org.junit.Test;

public class TestScreen extends QuizScreen {
    @Test
    public void main() {
        render();
    }

    @Override
    public Screen render() {
        //au vu de l'implémentation actuelle, ce test ne passe pas
        //impossible de répondre dans la fenêtre run, "This view is read-only"

        /*
        String[] possibleAnswers = {"Paris", "New York", "Tokyo", "le petit village de Y"};
        answerQuestion(1, "Quelle est la capitale de la France ?", possibleAnswers, "A", "Le petit village de Y est trop petit pour être la capitale. New York et Tokyo ne sont pas situées en France");
        */

        return null;
    }
}
