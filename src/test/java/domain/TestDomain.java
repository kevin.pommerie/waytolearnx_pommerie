package domain;

import org.junit.Assert;
import org.junit.Test;

import java.util.Locale;

public class TestDomain {

    @Test
    public void main() {
        int number = 1;
        String enonce = "ceci est l'énoncé";
        String goodAnswers = "A B C";
        String[] possibleAnswers = {"la réponse A", "la réponse B", "la réponse C", "la réponse D"};
        String explanation = "ceci est l'explication";

        Question questionTest = new Question(number, enonce, goodAnswers, possibleAnswers, explanation);
        questionTest.printQuestion();

        Answer choices = new Answer();
        choices.setContentAnswer("a B c");

        Assert.assertEquals(choices.getContentAnswer().toUpperCase(Locale.ROOT), goodAnswers);
    }
}
